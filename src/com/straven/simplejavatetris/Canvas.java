package com.straven.simplejavatetris;

import javax.swing.*;
import java.awt.*;

public class Canvas extends JPanel {
    @Override
    public void paint(Graphics g) {
        super.paint(g);
        for (int x = 0; x < GameConstants.FIELD_WIDTH; x++) {
            for (int y = 0; y < GameConstants.FIELD_HEIGHT; y++) {
                if (x < GameConstants.FIELD_WIDTH - 1 && y < GameConstants.FIELD_HEIGHT - 1) {
                    g.setColor(Color.lightGray);
                    g.drawLine((x+1)*GameConstants.BLOCK_SIZE-2, (y+1)*GameConstants.BLOCK_SIZE, (x+1)*GameConstants.BLOCK_SIZE+2, (y+1)*GameConstants.BLOCK_SIZE);
                    g.drawLine((x+1)*GameConstants.BLOCK_SIZE, (y+1)*GameConstants.BLOCK_SIZE-2, (x+1)*GameConstants.BLOCK_SIZE, (y+1)*GameConstants.BLOCK_SIZE+2);
                }
                if (GameConstants.mine[y][x] > 0) {
                    g.setColor(new Color(GameConstants.mine[y][x]));
                    g.fill3DRect(x*GameConstants.BLOCK_SIZE+1, y*GameConstants.BLOCK_SIZE+1, GameConstants.BLOCK_SIZE-1, GameConstants.BLOCK_SIZE-1, true);
                }
            }
        }
        if (SimpleJavaTetris.gameOver) {
            g.setColor(Color.white);
            for (int y = 0; y < GameConstants.GAME_OVER_MSG.length; y++) {
                for (int x = 0; x < GameConstants.GAME_OVER_MSG[y].length; x++) {
                    if (GameConstants.GAME_OVER_MSG[y][x] == 1) {
                        g.fill3DRect(x*11+18, y*11+160, 10, 10, true);
                    }
                }
            }
        } else {
            GameConstants.figure.paint(g);
        }
    }
}
