package com.straven.simplejavatetris;

import java.awt.*;
import java.util.ArrayList;
import java.util.Random;

public class Figure {
    private final ArrayList<Block> figure = new ArrayList<>();
    private final int[][] shape = new int[4][4];
    private final int size;
    private final int color;
    private int x = 3;
    private int y = 0;

    public Figure() {
        int type = new Random().nextInt(GameConstants.SHAPES.length);
        size = GameConstants.SHAPES[type][4][0];
        color = GameConstants.SHAPES[type][4][1];
        if (size == 4) {
            y = -1;
        }
        for (int i = 0; i < size; i++) {
            System.arraycopy(GameConstants.SHAPES[type][i], 0, shape[i], 0, GameConstants.SHAPES[type][i].length);
        }
        createFromShape();
    }

    public void createFromShape() {
        for (int xPos = 0; xPos < size; xPos++) {
            for (int yPos = 0; yPos < size; yPos++) {
                if (shape[yPos][xPos] == 1) {
                    figure.add(new Block(xPos + this.x, yPos + this.y));
                }
            }
        }
    }

    public boolean isTouchGround() {
        for (Block block : figure) {
            if (GameConstants.mine[block.getY() + 1][block.getX()] > 0) {
                return true;
            }
        }
        return false;
    }

    public boolean isCrossGround() {
        for (Block block : figure) {
            if (GameConstants.mine[block.getY()][block.getX()] > 0) {
                return true;
            }
        }
        return false;
    }

    public void leaveOnTheGround() {
        for (Block block : figure) {
            GameConstants.mine[block.getY()][block.getX()] = color;
        }
    }

    public boolean isTouchWall(int direction) {
        for (Block block : figure) {
            if (direction == GameConstants.LEFT && (block.getX() == 0 || GameConstants.mine[block.getY()][block.getX() - 1] > 0)) {
                return true;
            }
            if (direction == GameConstants.RIGHT && (block.getX() == GameConstants.FIELD_WIDTH - 1 || GameConstants.mine[block.getY()][block.getX() + 1] > 0)) {
                return true;
            }
        }
        return false;
    }

    public void move(int direction) {
        if (!isTouchWall(direction)) {
            int dx = direction - 38;
            for (Block block : figure) {
                block.setX(block.getX() + dx);
            }
            x += dx;
        }
    }

    public void stepDown() {
        for (Block block : figure) {
            block.setY(block.getY() + 1);
        }
        y++;
    }

    public void drop() {
        while (!isTouchGround()) {
            stepDown();
        }
    }

    public boolean isWrongPosition() {
        for (int x = 0; x < size; x++) {
            for (int y = 0; y < size; y++) {
                if (shape[y][x] == 1) {
                    if (y + this.y < 0) {
                        return true;
                    }
                    if (x + this.x < 0 || x + this.x > GameConstants.FIELD_WIDTH -1 ) {
                        return true;
                    }
                    if (GameConstants.mine[y + this.y][x + this.x] > 0) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public void rotateShape(int direction) {
        for (int i = 0; i < size/2; i++) {
            for (int j = i; j < size-1-i; j++) {
                if (direction == GameConstants.RIGHT) {
                    int tmp = shape[size-1-j][i];
                    shape[size-1-j][i] = shape[size-1-i][size-1-j];
                    shape[size-1-i][size-1-j] = shape[j][size-1-i];
                    shape[j][size-1-i] = shape[i][j];
                    shape[i][j] = tmp;
                } else {
                    int tmp = shape[i][j];
                    shape[i][j] = shape[j][size-1-i];
                    shape[j][size-1-i] = shape[size-1-i][size-1-j];
                    shape[size-1-i][size-1-j] = shape[size-1-j][i];
                    shape[size-1-j][i] = tmp;
                }
            }
        }
    }

    public void rotate() {
        rotateShape(GameConstants.RIGHT);
        if (!isWrongPosition()) {
            figure.clear();
            createFromShape();
        } else {
            rotateShape(GameConstants.LEFT);
        }
    }

    public void paint(Graphics g) {
        for (Block block : figure) {
            block.paint(g, color);
        }
    }
}
