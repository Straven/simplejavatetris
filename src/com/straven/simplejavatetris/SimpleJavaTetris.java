package com.straven.simplejavatetris;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SimpleJavaTetris extends JFrame {

    int gameScore = 0;
    Canvas canvas = new Canvas();
    public static boolean gameOver = false;

    private static final Logger LOGGER = Logger.getLogger(
            Thread.currentThread().getStackTrace()[0].getClassName() );

    public static void main(String[] args) {
        new SimpleJavaTetris().go();
    }

    SimpleJavaTetris() {
        setTitle(GameConstants.TITLE_OF_PROGRAM);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setBounds(GameConstants.START_LOCATION, GameConstants.START_LOCATION, GameConstants.FIELD_WIDTH * GameConstants.BLOCK_SIZE + GameConstants.FIELD_DX, GameConstants.FIELD_HEIGHT * GameConstants.BLOCK_SIZE + GameConstants.FIELD_DY);
        setResizable(false);
        canvas.setBackground(Color.black);
        addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (!gameOver) {
                    if (e.getKeyCode() == GameConstants.DOWN) {
                        GameConstants.figure.drop();
                    }
                    if (e.getKeyCode() == GameConstants.UP) {
                        GameConstants.figure.rotate();
                    }
                    if (e.getKeyCode() == GameConstants.LEFT || e.getKeyCode() == GameConstants.RIGHT) {
                        GameConstants.figure.move(e.getKeyCode());
                    }
                }
                canvas.repaint();
            }
        });
        add(BorderLayout.CENTER, canvas);
        setVisible(true);
        Arrays.fill(GameConstants.mine[GameConstants.FIELD_HEIGHT], 1);
    }

    public void go(){
        while (!gameOver) {
            try {
                Thread.sleep(GameConstants.SHOW_DELAY);
            } catch (Exception e) {
                LOGGER.log(Level.WARNING, "Something went wrong!", e);
            }
            canvas.repaint();
            checkFilling();
            if (GameConstants.figure.isTouchGround()) {
                GameConstants.figure.leaveOnTheGround();
                GameConstants.figure = new Figure();
                gameOver = GameConstants.figure.isCrossGround();
            } else {
                GameConstants.figure.stepDown();
            }
        }
    }

    public void checkFilling() {
        int row = GameConstants.FIELD_HEIGHT - 1;
        int countFillRows = 0;
        while (row > 0) {
            int filled = 1;
            for (int col = 0; col < GameConstants.FIELD_WIDTH; col++) {
                filled *= Integer.signum(GameConstants.mine[row][col]);
            }
            if (filled > 0) {
                countFillRows++;
                for (int i = row; i > 0; i--) {
                    System.arraycopy(GameConstants.mine[i-1], 0, GameConstants.mine[i], 0, GameConstants.FIELD_WIDTH);
                }
            } else {
                row--;
            }
        }
        if (countFillRows > 0) {
            gameScore += GameConstants.SCORES[countFillRows - 1];
            setTitle(gameScore + " : " + GameConstants.TITLE_OF_PROGRAM);
        }
    }


}
