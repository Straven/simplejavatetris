package com.straven.simplejavatetris;

import java.awt.*;

public class Block {
    private int x;
    private int y;

    public Block(int x, int y) {
        setX(x);
        setY(y);
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void paint(Graphics g, int color) {
        g.setColor(new Color(color));
        g.drawRoundRect(x*GameConstants.BLOCK_SIZE+1, y*GameConstants.BLOCK_SIZE+1, GameConstants.BLOCK_SIZE-2, GameConstants.BLOCK_SIZE-2, GameConstants.ARC_RADIUS, GameConstants.ARC_RADIUS);
    }
}
